import unittest
from typing import List

from generator.domain.isometric.isometric_grid_generator import IsometricGridGenerator
from generator.domain.square.square_grid_generator import Stroke


class TestSquareGrid(unittest.TestCase):
    generator = IsometricGridGenerator()
    image_width = 500
    spacing_width = 32
    spacing_height = 16
    image_height = 250
    width_by_height_ratio = 2

    def test_horizontal_spacing(self):
        # given

        # when
        strokes = self.generator.generate(image_width=self.image_width,
                                          spacing=self.spacing_width,
                                          image_height=self.image_height,
                                          width_by_height_ratio=self.width_by_height_ratio)
        # then
        self.__is_horizontal_spacing_equals(self.spacing_width, strokes)

    def __is_horizontal_spacing_equals(self, spacing: int, strokes: List[Stroke]):
        for i in range(1, len(strokes)):
            counter = 0
            for j in range(1, len(strokes)):
                if abs(strokes[i].starting_width - strokes[j].starting_width) == abs(spacing*2):
                    counter += 1
            self.assertTrue(counter > 0)


if __name__ == '__main__':
    unittest.main()
