import unittest
from typing import List

from generator.domain.square.square_grid_generator import SquareGridGenerator
from generator.domain.square.square_grid_generator import Stroke


class TestSquareGrid(unittest.TestCase):
    generator = SquareGridGenerator()
    image_width = 500
    spacing_width = 32
    spacing_height = 16
    image_height = 250

    def test_horizontal_spacing(self):
        # given

        # when
        strokes = self.generator.generate(image_width=self.image_width,
                                          spacing_width=self.spacing_width,
                                          spacing_height=self.spacing_height,
                                          image_height=self.image_height)
        # then
        self.__is_horizontal_spacing_equals(self.spacing_width, strokes)

    def test_vertical_spacing(self):
        # given

        # when
        strokes = self.generator.generate(image_width=self.image_width,
                                          spacing_width=self.spacing_width,
                                          spacing_height=self.spacing_height,
                                          image_height=self.image_height)
        # then
        self.__is_vertical_spacing_equals(self.spacing_height, strokes)

    def __is_horizontal_spacing_equals(self, spacing: int, strokes: List[Stroke]):
        for i in range(1, len(strokes)):
            if strokes[i].starting_height == 0 and strokes[i].finish_height == self.image_height:
                self.assertTrue(strokes[i].starting_width - strokes[i - 1].starting_width == spacing)

    def __is_vertical_spacing_equals(self, spacing: int, strokes: List[Stroke]):
        for i in range(1, len(strokes)):
            if strokes[i].starting_width == 0 and strokes[i].finish_width == self.image_width \
                    and strokes[i-1].starting_width == 0 and strokes[i-1].finish_width == self.image_width:
                self.assertTrue(strokes[i].starting_height - strokes[i - 1].starting_height == spacing)


if __name__ == '__main__':
    unittest.main()
