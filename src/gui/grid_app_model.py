from kivy.event import EventDispatcher
from kivy.properties import StringProperty, ListProperty


class GridAppModel(EventDispatcher):
    background_rgba_color = ListProperty([0.95, 0.95, 0.95, 1])
    font_rgba_color = ListProperty([0.2, 0.2, 0.2, 1])
    border_rgba_color = ListProperty([0.35, 0.35, 0.35, 1])
    alert_font_rgba_color = ListProperty([0.75, 0.35, 0.35, 1])
    input_font_rgba_color = ListProperty([0.05, 0.05, 0.05, 1])

