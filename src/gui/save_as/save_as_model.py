from kivy.event import EventDispatcher
from kivy.properties import BooleanProperty, StringProperty


class SaveAsModel(EventDispatcher):
    path = StringProperty('')

    def __init__(self, **kwargs):
        super(SaveAsModel, self).__init__(**kwargs)
