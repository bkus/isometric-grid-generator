from kivy.clock import Clock
from kivy.event import EventDispatcher
from kivy.properties import BooleanProperty, StringProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from plyer import filechooser
from gui.path_validator import PathValidator
from gui.save_as.save_as_model import SaveAsModel
from gui.validated_input.validated_path_input_model import ValidatedPathInputModel
import logging


class SaveAs(Button):
    __model = SaveAsModel()
    path = StringProperty(__model.path)
    action = ObjectProperty()

    def __init__(self, **kwargs):
        super(SaveAs, self).__init__(**kwargs)
        self.__model.bind(path=self.setter('path'))
        self.bind(path=self.__model.setter('path'))
        self.action = self.__execute

    def on_release(self) -> None:
        result = filechooser.save_file(path="D:\\test", filters=["*.png"])
        if len(result) > 0:
            self.path = self.__ensure_contains_extension(result[0])
            self.action()

    def __execute(self):
        logging.warning("save as stub wasn't overloaded")

    def __ensure_contains_extension(self, filename: str) -> str:
        result = filename
        if not self.__has_file_extension(".png", filename):
            result = filename + ".png"
        return result

    def __has_file_extension(self, extension: str, filename: str) -> bool:
        return filename[-4:] == extension
