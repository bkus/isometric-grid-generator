from kivy.properties import ListProperty, StringProperty, Clock
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
import logging

from gui.square.page_square_model import PageSquareModel


class PageSquare(BoxLayout):
    __model = PageSquareModel()

    def __init__(self, **kwargs):
        super(PageSquare, self).__init__(**kwargs)

    def generate(self):
        path: str = self.ids['square_path'].path
        width: int = int(self.ids['square_width'].text)
        height: int = int(self.ids['square_height'].text)
        square_spacing_width: int = int(self.ids['square_spacing_width'].text)
        square_spacing_height: float = float(self.ids['square_spacing_height'].text)
        logging.info(f'generating grid with parameters path: {path}, width: {width}, height: {height}, square_spacing_width: {square_spacing_width}, square_spacing_height: {square_spacing_height}')
        self.__model.generate_image(path, width, height, square_spacing_width, square_spacing_height)