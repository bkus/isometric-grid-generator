from kivy.properties import StringProperty

from generator.data.save_as_image import SaveAsImage
from generator.domain.image_aggregate import ImageAggregate
from generator.domain.square.square_grid_dto import SquareGridDto


class PageSquareModel:
    generator = None
    save_image: SaveAsImage = SaveAsImage()

    def generate_image(self, path: str, width: int, height: int, spacing_width: int, spacing_height: int) -> None:
        aggregate = ImageAggregate(height=height, width=width, path=path)
        grid_dto = SquareGridDto(spacing_width=spacing_width, spacing_height=spacing_height)
        aggregate.generate_square_grid(grid_dto)
        self.save_image.save(aggregate)
