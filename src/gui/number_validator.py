class NumberValidator:

    def __init__(self) -> None:
        super().__init__()

    def is_number(self, value: str) -> bool:
        dig = value.replace('.', '', 1)
        return dig.isdigit()
