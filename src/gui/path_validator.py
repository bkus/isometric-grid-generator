import os
import logging
import ntpath


class PathValidator:

    def __init__(self) -> None:
        super(PathValidator, self).__init__()

    def is_path_valid(self, value: str) -> bool:
        base_path = self.__remove_filename_from_path(value)
        filename = ntpath.basename(value)
        return self.__base_path_exists(base_path) and self.__valid_filename(filename) and not self.__file_exists(filename)

    def __remove_filename_from_path(self, value: str) -> str:
        return os.path.dirname(os.path.abspath(value))

    def __valid_filename(self, value: str) -> bool:
        result = len(value) > 0
        logging.debug(f'filename length longer than 0: : {str (result)}')
        return result

    def __file_exists(self, value: str) -> bool:
        result = os.path.isfile(value)
        logging.debug(f'does file exist: {str (result)}')
        return result

    def __base_path_exists(self, value: str) -> bool:
        result = os.path.exists(value)
        logging.debug(f'does path exist: {value}, {str (result)}')
        return result

