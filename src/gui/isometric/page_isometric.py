from kivy.properties import ListProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from gui.isometric.page_isometric_model import PageIsometricModel
from gui.number_validator import NumberValidator
import logging


class PageIsometric(BoxLayout):
    __model = PageIsometricModel()

    def __init__(self, **kwargs):
        super(PageIsometric, self).__init__(**kwargs)

    def generate(self):
        if not self.__validate():
            logging.warning("inserted values are in wrong format")
            return

        width: int = int(self.ids['isometric_width'].text)
        height: int = int(self.ids['isometric_height'].text)
        spacing: int = int(self.ids['isometric_spacing'].text)
        ratio: float = float(self.ids['isometric_ratio'].text)
        path: str = self.ids['isometric_path'].path
        logging.info(
            f'generating grid with parameters path: {path}, width: {width}, height: {height}, spacing: {spacing}, ratio: {ratio}')
        self.__model.generate_image(path, width, height, spacing, ratio)

    def __validate(self) -> bool:
        return (self.ids['isometric_width'].is_valid and
                self.ids['isometric_height'].is_valid and
                self.ids['isometric_spacing'].is_valid and
                self.ids['isometric_ratio'].is_valid)
