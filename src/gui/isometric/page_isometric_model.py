from kivy.event import EventDispatcher

from generator.data.save_as_image import SaveAsImage
from generator.domain.image_aggregate import ImageAggregate
from generator.domain.isometric.isometric_grid_dto import IsometricGridDto


class PageIsometricModel(EventDispatcher):
    generator = None
    save_image: SaveAsImage = SaveAsImage()

    def __init__(self, **kwargs):
        super(PageIsometricModel, self).__init__(**kwargs)

    def generate_image(self, path: str, width: int, height: int, spacing: int, ratio: float) -> None:
        aggregate = ImageAggregate(height=height, width=width, path=path)
        grid_dto = IsometricGridDto(spacing=spacing, width_by_height_ratio=ratio)
        aggregate.generate_isometric_grid(grid_dto)
        self.save_image.save(aggregate)
