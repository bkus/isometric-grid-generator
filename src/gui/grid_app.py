import kivy
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.app import App
from kivy.properties import ListProperty, Property
from gui.grid_app_model import GridAppModel
from gui.main.main_window_controller import MainWindowController
import logging

kivy.require('2.0.0')


class GridApp(App):
    __model = GridAppModel()
    background_rgba_color = ListProperty(__model.background_rgba_color)
    font_rgba_color = ListProperty(__model.font_rgba_color)
    border_rgba_color = Property(__model.border_rgba_color)
    alert_font_rgba_color = ListProperty(__model.alert_font_rgba_color)
    input_font_rgba_color = ListProperty(__model.input_font_rgba_color)

    def __init__(self, **kwargs):
        super(GridApp, self).__init__(**kwargs)
        Window.minimum_height = 500
        Window.minimum_width = 600

        self.load_kivy()
        self.bind_model()

    def build(self) -> MainWindowController:
        logging.basicConfig(filename='app.log', level=logging.DEBUG)
        logging.info('Starting')
        return MainWindowController()

    def say_hello(self):
        logging.info("hello world")

    def load_kivy(self):
        Builder.load_file('src/gui/styling/predefined.kv')
        Builder.load_file('src/gui/validated_input/validated_float_input.kv')
        Builder.load_file('src/gui/validated_input/validated_bounded_int_input.kv')
        Builder.load_file('src/gui/validated_input/validated_path_input.kv')
        Builder.load_file('src/gui/save_as/save_as.kv')
        Builder.load_file('src/gui/main/main_window.kv')
        Builder.load_file('src/gui/square/page_square.kv')
        Builder.load_file('src/gui/isometric/page_isometric.kv')

    def bind_model(self):
        self.__model.bind(background_rgba_color=self.setter('background_rgba_color'),
                          font_rgba_color=self.setter('font_rgba_color'),
                          input_font_rgba_color=self.setter('input_font_rgba_color'),
                          border_rgba_color=self.setter('border_rgba_color'),
                          alert_font_rgba_color=self.setter('alert_font_rgba_color'))
        self.bind(background_rgba_color=self.__model.setter('background_rgba_color'),
                          font_rgba_color=self.__model.setter('font_rgba_color'),
                          input_font_rgba_color=self.__model.setter('input_font_rgba_color'),
                          border_rgba_color=self.__model.setter('border_rgba_color'),
                          alert_font_rgba_color=self.__model.setter('alert_font_rgba_color'))
