from kivy.event import EventDispatcher
from kivy.properties import StringProperty, BooleanProperty


class ValidatedFloatInputModel(EventDispatcher):
    is_valid = BooleanProperty(False)
    input_description = StringProperty('')

    def __init__(self, **kwargs):
        super(ValidatedFloatInputModel, self).__init__(**kwargs)
