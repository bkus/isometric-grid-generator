from kivy.clock import Clock
from kivy.event import EventDispatcher
from kivy.properties import BooleanProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout

from gui.path_validator import PathValidator
from gui.validated_input.validated_path_input_model import ValidatedPathInputModel
import logging


class ValidatedPathInput(BoxLayout):
    __model = ValidatedPathInputModel()
    is_valid = BooleanProperty(__model.is_valid)
    input_description = StringProperty(__model.input_description)
    text = StringProperty(__model.text)

    def __init__(self, **kwargs):
        super(ValidatedPathInput, self).__init__(**kwargs)
        self.__model.bind(is_valid=self.setter('is_valid'),
                          input_description=self.setter('input_description'),
                          text=self.setter('text'))
        self.bind(is_valid=self.__model.setter('is_valid'),
                  input_description=self.__model.setter('input_description'),
                  text=self.__model.setter('text'))
        self.register_event_type('on_text')

    def on_text(self, instance, value: str) -> None:
        self.__update_root_text(value)
        self.__validate_path(value)

    def __update_root_text(self, value: str) -> None:
        self.text = value

    def __validate_path(self, path: str) -> None:
        result = PathValidator().is_path_valid(path)
        logging.info(f'path validation for ({path}): {result}')
        self.__model.is_valid = result
