from kivy.properties import ListProperty, StringProperty, Clock, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout

from gui.number_validator import NumberValidator
from gui.square.page_square_model import PageSquareModel
from gui.validated_input.validated_float_input_model import ValidatedFloatInputModel


class ValidatedFloatInput(BoxLayout):
    __model = ValidatedFloatInputModel()
    is_valid = BooleanProperty(__model.is_valid)
    input_description = StringProperty(__model.input_description)

    def __init__(self, **kwargs):
        super(ValidatedFloatInput, self).__init__(**kwargs)
        self.__model.bind(is_valid=self.setter('is_valid'),
                          input_description=self.setter('input_description'))
        self.bind(is_valid=self.__model.setter('is_valid'),
                  input_description=self.__model.setter('input_description'))
        self.register_event_type('on_text')

    def on_text(self, instance, value: str):
        self.__model.is_valid = NumberValidator().is_number(value)
