from kivy.event import EventDispatcher
from kivy.properties import StringProperty, BooleanProperty, NumericProperty

from gui.number_validator import NumberValidator


class ValidatedBoundedIntInputModel(EventDispatcher):
    is_valid = BooleanProperty(True)
    input_description = StringProperty('')
    min = NumericProperty(0)
    max = NumericProperty(10000)
    text = StringProperty('1000')

    def __init__(self, **kwargs):
        super(ValidatedBoundedIntInputModel, self).__init__(**kwargs)
        self.register_event_type('on_text')

    def on_text(self, dispatcher, value):
        self.is_valid = self.__validate_number(value)

    def __validate_number(self, value: str) -> None:
        return NumberValidator().is_number(value) and self.min <= int(value) <= self.max