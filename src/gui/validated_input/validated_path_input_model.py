from kivy.event import EventDispatcher
from kivy.properties import BooleanProperty, StringProperty


class ValidatedPathInputModel(EventDispatcher):
    is_valid = BooleanProperty(False)
    input_description = StringProperty('')
    text = StringProperty('')

    def __init__(self, **kwargs):
        super(ValidatedPathInputModel, self).__init__(**kwargs)
