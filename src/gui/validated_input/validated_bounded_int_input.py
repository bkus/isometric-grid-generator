from kivy.properties import ListProperty, StringProperty, Clock, BooleanProperty, NumericProperty, AliasProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout

from gui.number_validator import NumberValidator
from gui.square.page_square_model import PageSquareModel
from gui.validated_input.validated_bounded_int_input_model import ValidatedBoundedIntInputModel
from gui.validated_input.validated_float_input_model import ValidatedFloatInputModel


class ValidatedBoundedIntInput(BoxLayout):
    __model = ValidatedBoundedIntInputModel()
    min = NumericProperty(__model.min)
    max = NumericProperty(__model.max)
    is_valid = BooleanProperty(__model.is_valid)
    input_description = StringProperty(__model.input_description)
    text = StringProperty(__model.text)

    def __init__(self, **kwargs):
        super(ValidatedBoundedIntInput, self).__init__(**kwargs)
        self.__model = ValidatedBoundedIntInputModel()
        self.__model.bind(is_valid=self.setter('is_valid'),
                          input_description=self.setter('input_description'),
                          text=self.setter('text'),
                          min=self.setter('min'),
                          max=self.setter('max'))
        self.bind(is_valid=self.__model.setter('is_valid'),
                  input_description=self.__model.setter('input_description'),
                  text=self.__model.setter('text'),
                  min=self.__model.setter('min'),
                  max=self.__model.setter('max'))
