# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


# Press the green button in the gutter to run the script.
from gui.grid_app import GridApp
from gui.isometric.page_isometric import PageIsometric

if __name__ == '__main__':
    x = PageIsometric()
    GridApp().run()

