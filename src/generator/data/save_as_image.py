from PIL import Image, ImageDraw
from PIL.ImageDraw import Draw

from generator.domain.image_aggregate import ImageAggregate
from generator.domain.isometric.stroke import Stroke


class SaveAsImage:
    def save(self, image_aggregate: ImageAggregate):
        image = Image.new('RGBA', (image_aggregate.width, image_aggregate.height))
        image_drawable = Draw(image)
        self.__apply_strokes(image_drawable, image_aggregate.strokes)
        image.save(image_aggregate.path)

    def __apply_strokes(self, image_drawable: ImageDraw, strokes: [Stroke]):
        for stroke in strokes:
            image_drawable.line((
                stroke.starting_width,
                stroke.starting_height,
                stroke.finish_width,
                stroke.finish_height))
