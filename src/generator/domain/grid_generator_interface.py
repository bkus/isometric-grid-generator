from typing import List

from generator.domain.isometric.stroke import Stroke


class GridGeneratorInterface:
    def generate(self) -> List[Stroke]:
        pass
