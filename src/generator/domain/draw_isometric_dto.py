from dataclasses import dataclass # intellij idea falsely reports this as an error

@dataclass
class ImageDto:
    filename: str = "../test.png"
    width: int = 800
    height: int = 800