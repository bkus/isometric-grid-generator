from dataclasses import dataclass  # intellij idea falsely reports this as an error


@dataclass
class SquareGridDto:
    spacing_width: int = 32
    spacing_height: int = 32
