from typing import List

from generator.domain.isometric.stroke import Stroke
from generator.domain.square.square_grid_dto import SquareGridDto


class SquareGridGenerator:
    def generate(self, image_width, image_height, spacing_width, spacing_height) -> List[Stroke]:
        result: List[Stroke] = []
        iteration = 0
        while spacing_width * iteration < image_width+1:
            width = spacing_width*iteration
            result.append(self.__draw_vertical_line(width=width, max_height=image_height))
            iteration += 1

        iteration = 0
        while spacing_height * iteration < image_height+1:
            height = spacing_height*iteration
            result.append(self.__draw_horizontal_line(height=height, max_width=image_width))
            iteration += 1
        return result

    def __draw_vertical_line(self, width: int, max_height: int) -> Stroke:
        return Stroke(starting_width=width, starting_height=0, finish_width=width, finish_height=max_height)

    def __draw_horizontal_line(self, height: int, max_width: int) -> Stroke:
        return Stroke(starting_width=0, starting_height=height, finish_width=max_width, finish_height=height)
