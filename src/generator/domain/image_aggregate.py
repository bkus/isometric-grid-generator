from typing import List

from dataclasses import dataclass, field

from generator.domain.draw_isometric_dto import ImageDto
from generator.domain.isometric.isometric_grid_dto import IsometricGridDto
from generator.domain.isometric.isometric_grid_generator import IsometricGridGenerator
from generator.domain.isometric.stroke import Stroke
from generator.domain.square.square_grid_dto import SquareGridDto
from generator.domain.square.square_grid_generator import SquareGridGenerator


@dataclass
class ImageAggregate:
    width: int
    height: int
    path: str
    strokes: List[Stroke] = field(default_factory=list)
    isometric_generator: IsometricGridGenerator = field(default_factory=IsometricGridGenerator)
    square_generator: SquareGridGenerator = field(default_factory=SquareGridGenerator)

    def generate_isometric_grid(self, grid_dto: IsometricGridDto) -> None:
        self.strokes = self.isometric_generator.generate(image_width=self.width,
                                                         image_height=self.height,
                                                         spacing=grid_dto.spacing,
                                                         width_by_height_ratio=grid_dto.width_by_height_ratio)

    def generate_square_grid(self, grid_dto: SquareGridDto):
        self.strokes = self.square_generator.generate(image_width=self.width,
                                                      image_height=self.height,
                                                      spacing_width=grid_dto.spacing_width,
                                                      spacing_height=grid_dto.spacing_height)
