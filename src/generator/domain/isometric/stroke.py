from dataclasses import dataclass

@dataclass
class Stroke:
    starting_width: int
    starting_height: int
    finish_height: int
    finish_width: int
