from typing import List

from generator.domain.isometric.isometric_grid_dto import IsometricGridDto
from generator.domain.isometric.stroke import Stroke


class IsometricGridGenerator:
    def generate(self, spacing: int, image_width: int, image_height: int, width_by_height_ratio: float) -> List[Stroke]:
        strokes: List[Stroke] = []
        iteration = 0
        # because strokes are ending at different X than they start, We should generate the offset too
        offset_x = width_by_height_ratio * image_height
        while spacing * iteration - offset_x < image_width + offset_x:
            at_width = spacing * iteration - offset_x
            strokes.append(self.__draw_diagonal_line(at_width=at_width,
                                                     width_by_height_ratio=width_by_height_ratio,
                                                     max_height=image_height,
                                                     right_facing=True))
            strokes.append(self.__draw_diagonal_line(at_width=at_width,
                                                     width_by_height_ratio=width_by_height_ratio,
                                                     max_height=image_height,
                                                     right_facing=False))
            iteration += 1
        return strokes

    def __draw_diagonal_line(self, at_width: float, max_height: int, width_by_height_ratio: float, right_facing: bool) -> Stroke:
        if right_facing:
            finish_width = width_by_height_ratio * max_height + at_width
        else:
            finish_width = -(width_by_height_ratio * max_height) + at_width

        return Stroke(starting_width=at_width,
                      starting_height=0,
                      finish_width=finish_width,
                      finish_height=max_height)
