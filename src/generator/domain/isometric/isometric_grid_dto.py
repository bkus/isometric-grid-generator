from dataclasses import dataclass  # intellij idea falsely reports this as an error


@dataclass
class IsometricGridDto:
    spacing: int = 120
    width_by_height_ratio: float = 1.618
