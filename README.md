# isometric-grid-generator

## Requirements 
- python 3.7.x
- virtualenv
- dependencies from requirements.txt
- for development, we recommend pycharm.

## setup Pycharm
- mark directory as sources root  
  ![img](docs_assets/img.png)
- install python 3.7. For windows the easiest way is to use Microsoft Store.
- to enable virtualEnv  
  `python -m pip install --upgrade pip wheel setuptools virtualenv`  
  `File -> Settings -> Project -> Python Interpreter`  
  ![img](docs_assets/img_1.png)  
  ![img](docs_assets/img_2.png)  
  `click add -> select virtualenv -> set base interpreter to python 3.7.X `  
- install dependencies `pip install -r requirements.txt`

## sources
- docs https://kivy.org/doc/stable/
- google fonts https://fonts.google.com/


## license
- fonts are licensed under the Open Font License. https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
